from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import action

# views.py
from rest_framework import viewsets

from .serializers import SupplierSerializer
from .models import Supplier

from .serializers import UserSupplierSerializer, UserSupplierSerializer
from .models import UsersSupplier


class SupplierViewSet(viewsets.ModelViewSet):
    queryset = Supplier.objects.all().order_by('name')
    serializer_class = SupplierSerializer

    @action(detail=False, methods=['GET'])
    def getSupplier(self, request, *args, **kwargs):
        own = self.request.user
        supplier = UsersSupplier.objects.filter(user=own)
        ownerser = UserSupplierSerializer(supplier[0],many=False)
        return Response(ownerser.data)

class UsersSupplierViewSet(viewsets.ModelViewSet):
    queryset = UsersSupplier.objects.all().order_by('name')
    serializer_class = UserSupplierSerializer
