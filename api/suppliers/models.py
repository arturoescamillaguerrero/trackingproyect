from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

# Create your models here.
class Supplier(models.Model):


    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False,max_length=100)
    address = models.CharField(null=False,max_length=100) 
    contact = models.CharField(null=False,max_length=100)
    phone = models.CharField(null=False,max_length=100) 
    email = models.CharField(null=False,max_length=100)
    giro = models.CharField(null=False,max_length=100)   
    status = models.BooleanField(default=True)
    supplier_pic = models.ImageField(upload_to = 'supplier/', default = 'supplier/None/no-img.jpg')
    registered_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.name

class UsersSupplier(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    supplier = models.ForeignKey(Supplier,on_delete=models.CASCADE)
