# myapi/urls.py
from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()
router.register(r'proveedores',SupplierViewSet, 'proveedores')
urls = router.urls


router_sup = DefaultRouter()
router_sup.register(r'userproveedores',UsersSupplierViewSet, 'userproveedores')
urls_sup = router_sup.urls