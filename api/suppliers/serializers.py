from rest_framework import serializers
from .models import Supplier, UsersSupplier
from api.clientes.serializers import UserSerializer

class SupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = '__all__'

class UserSupplierSerializer(serializers.ModelSerializer):
    supplier = SupplierSerializer(read_only=True,many=False)
    user = UserSerializer(read_only=True,many=False)
    class Meta:
        model = UsersSupplier
        fields = '__all__'
