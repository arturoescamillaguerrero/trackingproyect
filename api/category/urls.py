# myapi/urls.py
from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()
router.register(r'categoria',CategoryViewSet, 'categoria')
urls = router.urls