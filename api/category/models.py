from django.db import models
from datetime import datetime

# Create your models here.

class Category(models.Model):
  

    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False,max_length=100)
    description = models.TextField()
    status = models.BooleanField(default=True)
    category_pic = models.ImageField(upload_to = 'category/', default = 'category/None/no-img.jpg')
    registered_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.name
