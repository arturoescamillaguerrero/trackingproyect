from rest_framework import serializers
from .models import Project, Status_Delivery, tracking
from api.clientes.serializers import ClientSerializer
from api.suppliers.serializers import SupplierSerializer
from api.category.serializers import CategorySerializer


class StatusDeliverySerializer(serializers.ModelSerializer):
  
    class Meta:
        model = Status_Delivery
        fields = '__all__'



class ProjectSerializer(serializers.ModelSerializer):
    tracks = StatusDeliverySerializer(many=True, read_only=True)
    client = ClientSerializer(read_only=True)
    supplier = SupplierSerializer(read_only=True)
    category = CategorySerializer(read_only=True)

    class Meta:
        model = Project
        fields = '__all__'


class TrackSerializer(serializers.ModelSerializer):
    #status = StatusDeliverySerializer(read_only=True)
    #project = ProjectSerializer(read_only=True)
    class Meta:
        model = tracking
        fields = '__all__'
    
