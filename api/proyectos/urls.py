# myapi/urls.py
from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()
router.register(r'proyectos', ProjectViewSet,'proyectos')

router_del = DefaultRouter()
router_del.register(r'estatus', StatusDeliveryViewSet,'estatus')

router_track = DefaultRouter()
router_track.register(r'track', TrackingViewSet,'track')

urls = router.urls
urls_del = router_del.urls
urls_track = router_track.urls