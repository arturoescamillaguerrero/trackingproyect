from django.db import models
from django.contrib import admin
from datetime import datetime
from api.clientes.models import Client
from api.suppliers.models import Supplier
from api.category.models import Category
# from api.tracking.models import tracking_inline


# Create your models here.

class Project(models.Model):
  

    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False,max_length=100)
    quantity = models.IntegerField(null=True)
    specifications = models.TextField()
    delivery_address = models.CharField(null=False,max_length=100)
    delivery_date = models.DateTimeField(null=True)
    status = models.BooleanField(default=True)
    registered_on = models.DateTimeField(default=datetime.now, blank=True)
    client = models.ForeignKey(Client,on_delete=models.CASCADE)
    supplier = models.ForeignKey(Supplier,on_delete=models.CASCADE)
    category = models.ForeignKey(Category,on_delete=models.CASCADE)
  
    def __str__(self):
        return self.name


class Status_Delivery(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False,max_length=100)
    status_del = models.BooleanField(default=True)
    registered_on = models.DateTimeField(default=datetime.now, blank=True)
    projects = models.ManyToManyField(Project, related_name = 'tracks', through='tracking')
    

    def __str__(self):
        return self.name

class tracking(models.Model):
    
   
    comment = models.TextField()
    status_track = models.BooleanField(default=True)
    registered_on = models.DateTimeField(default=datetime.now, blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    status = models.ForeignKey(Status_Delivery, on_delete=models.CASCADE)
    track_pic = models.ImageField(upload_to = 'track/', default = 'track/None/no-img.jpg')

    def __str__(self):
        return self.comment


class tracking_inline(admin.TabularInline):
    model = tracking
    extra = 1

class projectAdmin(admin.ModelAdmin):
    inlines = (tracking_inline,)
class statusAdmin(admin.ModelAdmin):
    inlines = (tracking_inline,)
