from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import status

# views.py
from rest_framework import viewsets
from ..permissions.permissions import *
from ..clientes.models import UsersClient, Client
from ..clientes.serializers import UserClientSerializer
from ..suppliers.models import Supplier, UsersSupplier
from ..suppliers.serializers import SupplierSerializer,UserSupplierSerializer

from .serializers import ProjectSerializer, StatusDeliverySerializer, TrackSerializer, ClientSerializer
from .models import Project, Status_Delivery, tracking


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all().order_by('name')
    permission_classes = (IsCreationOrIsAuthenticated,)
    serializer_class = ProjectSerializer

    @action(detail=False, methods=['GET'], serializer_class=UserClientSerializer)
    def getclientproyects(self, request, *args, **kwargs):
        owner = self.request.user
        uc = UsersClient.objects.filter(user=owner)
        if uc:
            cp = Project.objects.filter(client=uc[0].client)
            if cp:
                cps = ProjectSerializer(cp,many=True)
                return Response(cps.data)
            else:
                return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)
    
    @action(detail=False, methods=['GET'], serializer_class=TrackSerializer)
    def gettrackingproyects(self, request, *args, **kwargs):
        #owner = self.request.user
        proyect_id = int(kwargs['proyect_id'])
        pr = Project.objects.filter(id=proyect_id)
        if pr:
            tr = tracking.objects.filter(project=pr[0])
            if tr:
                trs = TrackSerializer(tr,many=True)   
                return Response(trs.data)
            else:
                return Response(status=status.HTTP_204_NO_CONTENT)

        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

       
    
    @action(detail=False, methods=['GET'], serializer_class=UserClientSerializer)
    def getsuppliersproyects(self, request, *args, **kwargs):
        owner = self.request.user
        uc = UsersSupplier.objects.filter(user=owner)
        if uc:
            cp = Project.objects.filter(supplier=uc[0].supplier)
            if cp:
                cps = ProjectSerializer(cp,many=True)
                return Response(cps.data)
            else:
                return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)
        

class StatusDeliveryViewSet(viewsets.ModelViewSet):
    queryset = Status_Delivery.objects.all().order_by('name')
    permission_classes = (IsCreationOrIsAuthenticated,)
    serializer_class = StatusDeliverySerializer

class TrackingViewSet(viewsets.ModelViewSet):
    queryset = tracking.objects.all().order_by('comment')
    permission_classes = (IsCreationOrIsAuthenticated,)
    serializer_class = TrackSerializer
