from django.shortcuts import render

# views.py
from rest_framework import viewsets

from .serializers import LoginSerializer

from django.contrib.auth.models import Group, User
from rest_framework import viewsets
from .models import *
from django.contrib.auth import authenticate
from .serializers import *
from rest_framework.permissions import AllowAny
from rest_framework import generics, status

from rest_framework.response import Response
from rest_framework.views import APIView
from oauth2_provider.models import AccessToken
from datetime import timedelta



class LoginViewSet(generics.GenericAPIView):
    permission_classes = [AllowAny]
    serializer_class = LoginSerializer

    def post(self, request):
        s = LoginSerializer(data=request.data)
        if s.is_valid():
            try:
                user = authenticate(
                    username=s.data['username'],
                    password=s.data['password']
                )
            except Exception as e:
                content = {'error':str(e)}
                return Response(content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            if user is not None:
                client = Application.objects.get(user=user)
                response_data = {
                        'client_id': client.client_id,
                        'client_secret': client.client_secret,
                        'user_id':user.id
                }
                return Response(response_data)
            else:
                return Response({'error' : 'Wrong username or password'})
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)

class WhoisToken(generics.GenericAPIView):
    permission_classes = [AllowAny]
    serializer_class = WhoisSerializer
    def get(self, request):
        s = WhoisSerializer(data=request.query_params)
        if s.is_valid():
          token_value = s.data['token']
          atoken = AccessToken.objects.get(token=token_value)
          usuario = atoken.user
          username = usuario.username
          lista_grupos = [x.name for x in usuario.groups.all()]
          if usuario.is_superuser:
            lista_grupos.append("admin")
            response_data = {
              "data": {
                'name' : username,
                "roles": lista_grupos},              
          }
          return Response(response_data, status=status.HTTP_200_OK)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)

# class LoginViewSet(generics.GenericAPIView):
#     permission_classes = [AllowAny]
#     serializer_class = LoginSerializer
# ​    def post(self, request):
#         s = LoginSerializer(data=request.data)
#         if s.is_valid():
#             try:
#                 user = authenticate(
#                     username=s.data['username'],
#                     password=s.data['password']
#                 )
#             except Exception as e:
#                 content = {'error': str(e)}
#                 return Response(content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
# ​
#             if user is not None:
#                 client = Application.objects.get(user=user)
#                 response_data = {
#                     'client_id': client.client_id,
#                     'client_secret': client.client_secret,
#                     'user_id': user.id
#                 }
#                 return Response(response_data)
#             else:
#                 return Response({'error': 'Wrong username or password'},
#                                 status=status.HTTP_401_UNAUTHORIZED)
#         else:
#             return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
