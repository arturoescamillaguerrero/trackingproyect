from rest_framework import serializers
from oauth2_provider.models import Application, AccessToken
from django.utils import timezone

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=200)
    password = serializers.CharField(max_length=200)

class WhoisSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=200, required=True)
    def validate_token(self, value):
      try:
        t = AccessToken.objects.get(token=value)
        ahora = timezone.now()
        if (ahora > t.expires):
          raise serializers.ValidationError('Token no valido')
      except AccessToken.DoesNotExist:
        raise serializers.ValidationError('Token no valido')
      return value

class LogoutSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=200, required=True)