from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Client(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False,max_length=100)
    contact = models.CharField(null=False,max_length=100)
    email = models.CharField(null=False,max_length=100)
    status = models.BooleanField(default=True)
    client_pic = models.ImageField(upload_to = 'client/', default = 'client/None/no-img.jpg')
    registered_on = models.DateTimeField(default=datetime.now, blank=True)
    
    def __str__(self):
        return self.name


class UsersClient(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    client = models.ForeignKey(Client,on_delete=models.CASCADE)

   

# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         UsersClient.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.usersclient.save()