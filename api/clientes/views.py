from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import action

# views.py
from rest_framework import viewsets
from ..permissions.permissions import *


from .serializers import ClientSerializer,UserSerializer
from .serializers import UserClientSerializer
from .models import Client
from .models import UsersClient
from ..proyectos.models import Project
from ..proyectos.serializers import ProjectSerializer



class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    permission_classes = (IsCreationOrIsAuthenticated,)
    queryset = Client.objects.all().order_by('name')


    @action(detail=False, methods=['GET'])
    def getClient(self, request, *args, **kwargs):
        own = self.request.user
        client = UsersClient.objects.filter(user=own)
        ownerser = UserClientSerializer(client[0],many=False)
        return Response(ownerser.data)
    
    


class UserClientViewSet(viewsets.ModelViewSet):
    serializer_class = UserClientSerializer
    permission_classes = (IsCreationOrIsAuthenticated,)
    queryset = UsersClient.objects.all().order_by('user').select_related('client')
    
    



    
