from rest_framework import serializers
from .models import Client
from .models import UsersClient
from django.contrib.auth.models import Group, User


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(read_only=True,many=True)
    class Meta:
        model = User
        fields = ('id','last_login' , 'is_superuser' , 'username' , 'first_name' , 'last_name' , 'email' , 'is_staff', 'is_active' , 'date_joined' , 'groups')

class UserClientSerializer(serializers.ModelSerializer):
    client = ClientSerializer(read_only=True,many=False)
    user = UserSerializer(read_only=True,many=False)
    class Meta:
        model = UsersClient
        fields = '__all__'
