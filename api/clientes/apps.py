from django.apps import AppConfig


class clientesConfig(AppConfig):
    name = 'api.clientes'

class userclientesConfig(AppConfig):
    name = 'api.userclientes'
