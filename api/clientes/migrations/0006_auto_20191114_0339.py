# Generated by Django 2.2.3 on 2019-11-14 03:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('clientes', '0005_auto_20191114_0227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usersclient',
            name='client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clientes.Client'),
        ),
    ]
