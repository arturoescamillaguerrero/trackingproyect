# Generated by Django 2.2.3 on 2019-11-13 00:21

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('clientes', '0003_auto_20191112_2353'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ProfileClient',
            new_name='UsersClient',
        ),
    ]
