# myapi/urls.py
from rest_framework.routers import DefaultRouter
from .views import *

router = DefaultRouter()
router.register(r'clientes',ClientViewSet,'clientes')

router_user = DefaultRouter()
router_user.register(r'userclientes',UserClientViewSet,'userclientes')

urls = router.urls
urls_user = router_user.urls