from rest_framework import permissions

class IsCreationOrIsAuthenticated(permissions.BasePermission):
    def has_permission(self,request,view):
        if not request.user.is_authenticated:
            if view.action == 'create':
                return True
            return False
        return True
class IsOwnedByUser(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.id == request.user.id

class IsSuperUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser