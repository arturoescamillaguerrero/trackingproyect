from django.db import models
from datetime import datetime

# Create your models here.
class Asistencia(models.Model):


    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False,max_length=100)
    email = models.CharField(null=False,max_length=100)
    comments = models.CharField(null=False,max_length=500)   
    asistir = models.IntegerField(default=True)
    registered_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.name
