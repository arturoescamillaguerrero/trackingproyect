from django.shortcuts import render

# views.py
from rest_framework import viewsets

from .serializers import AsistenciaSerializer
from .models import Asistencia


class AsistenciaViewSet(viewsets.ModelViewSet):
    queryset = Asistencia.objects.all().order_by('name')
    serializer_class = AsistenciaSerializer
