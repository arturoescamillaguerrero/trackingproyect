from django.apps import AppConfig


class asistenciaConfig(AppConfig):
    name = 'api.asistencia'
