"""tracking_proyect URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.urls import path
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

from . import views
from api.clientes.urls import urls as clientesurls
from api.clientes.urls import urls_user as userclientesurls
from api.proyectos.urls import urls as proyectosurls
from api.proyectos.urls import urls_del as deliveryurls
from api.proyectos.urls import urls_track as trackurls
from api.suppliers.urls import urls as suppliersurls
from api.suppliers.urls import urls_sup as userssuppliersurls
from api.asistencia.urls import urls as asistenciaurls
from api.category.urls import urls as categoriasurls
from api.login.views import LoginViewSet, WhoisToken
from api.proyectos.views import TrackingViewSet,ProjectViewSet


# schema_view = get_swagger_view(title="Swagger Docs")

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/v0/login/$',LoginViewSet.as_view(),name='api_login'),
    url(r'^api/v0/whoistoken/$',WhoisToken.as_view(),name='api_whois_token'),
    url(r'^api/v0/',include(clientesurls)),
    url(r'^api/v0/',include(userclientesurls)),
    url(r'^api/v0/',include(proyectosurls)),
    url(r'^api/v0/',include(deliveryurls)),
    url(r'^api/v0/',include(trackurls)),
    url(r'^api/v0/',include(suppliersurls)),
    url(r'^api/v0/',include(userssuppliersurls)),
    url(r'^api/v0/',include(asistenciaurls)),
    url(r'^api/v0/',include(categoriasurls)),
   
    url(r'^o/',include('oauth2_provider.urls',namespace='oauth2_provider')),

    url(r'^apiref/',views.SwaggerSchemaView.as_view()),
    path('track/gettrackingproyects/<int:proyect_id>/', ProjectViewSet.as_view({"get": "gettrackingproyects"})),
    # path('admin/', admin.site.urls),
    # path('', include('api.clientes.urls')),
    # path('', include('api.proyectos.urls')),
    # path('', include('api.suppliers.urls')),
    # path('apiref/', schema_view),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
